import numpy as np
import pdb

'''
    ;This function is based on Christian Hummel's 2001 Michelson School
    ;web document. It's designed to be used directly for fitting...
    ;Updates from original: vectorized multiple times
    ;params are... 
    ; T0: The time of periastron passage
    ; P:  The period
    ; a:  The semi-major axis
    ; e:  The eccentricity
    ; n:  Capital omega ( an orbital angle )
    ; w:  Little omega
    ; i:  The inclination
    ;NB for differentiation, see mathematica/binary_diff.nb
    ;Done some timing tests on my laptop: this procedure is dominated
    ;by calculation, not interpretation when there are more than 30 jds.

    ;Meaning of negative numbers:
    ;Negative eccentricity: change \omega by 180 degrees and T by half a
    ; period.
    ;Negative inclination: the same as positive inclination!
    
    ACR converted it to python from IDL
'''

def binpos(params,jds,deriv=False):
    #projfact, and vr are outputs I think
    ##deriv not tested yet
     
     
    t = jds-params[0]
    P = params[1]
    a = params[2]
    e = abs(params[3])
    n = params[4]*np.pi/180.
    w = params[5]*np.pi/180.
    i = params[6]*np.pi/180.
    
    #Allow a negative eccentricity to have the obvious meaning.
    if params[3] < 0: 
        t += P/2.0
        w += np.pi
    
    
    #The mean anomaly 
    #(p,t) -> M 
    dMdt = -2*np.pi/P #- sign because time _since_ periastron passage.
    dMdp = -2*np.pi*t/P**2 #NB Can be very large.
    M = 2*np.pi*np.mod(t,P)/P
    #The eccentric anomaly, M = E - esinE
    #(M,e) -> (bE,e) ;Tr2
    bE = M+e*np.sin(M)+e**2/2*np.sin(2*M)
    for k in range(4): bE = bE + (M-bE+e*np.sin(bE))/(1-e*np.cos(bE))
    
    #The `true anomaly'. With a pi ambiguity,
    #(bE,e) -> (nu,e) ;Tr3
    nu=2*np.arctan2(np.sqrt((1+e)/(1-e))*np.sin(bE/2), np.cos(bE/2))
##    chk = np.where(np.isnan(nu)==True)
    
    alpha = nu + w  
    
    if deriv != False: ##untested
        dbEdM = 1./(1-e*np.cos(bE))
        dbEde =  np.sin(bE)/(1-e*np.cos(bE))
        #Derivatives are now for alpha (just an offset from nu)
        dnude  = np.sin(bE)/(e-1)/np.sqrt((1+e)/(1-e))/(e*np.cos(bE)-1)
        dnudbE = (e-1)*np.sqrt((1+e)/(1-e))/(e*np.cos(bE) - 1)
        dAdp = dnudbE*dbEdM*dMdp
        dAdt = dnudbE*dbEdM*dMdt
        dAde = dnudbE*dbEde + dnude
    
    #Final calculations (square brackets mean trivial):
    #(alpha,e,i) [+a,n] -> (rho,theta) Tr5
    #We have dAd(p,t,e,w), with alpha=A. Only complex for
    #e, where we need:
    #drhode = drhodA.dAde + drhodnu.dAde + drhode
    #dthde = dthdA.dAde + dthde
    #Also, drhodp = drhodA.dAdp etc...
    #!!! Much of the following can be sped-up by pre-calculation.

    
    sqtmp = np.sqrt(np.cos(alpha)**2+np.sin(alpha)**2*np.cos(i)**2)
    rho=a*(1-e**2)/(1+e*np.cos(nu))*sqtmp
    theta=np.arctan2(np.sin(alpha)*np.cos(i),np.cos(alpha))+n
    
    if deriv != False:
        drhodA = a*(e**2-1)/(1+e*np.cos(nu))*np.cos(alpha)*np.sin(alpha)*(np.sin(i))**2/sqtmp
        drhodnu = -a*e*(e**2-1)*np.sin(nu)/(1+e*np.cos(nu))**2*sqtmp
        drhode =  -a*(2*e+(1+e**2)*np.cos(nu))*sqtmp/(1 + e*np.cos(nu))**2
        drhodi = -a*(1-e**2)/(1+e*np.cos(nu))*np.cos(i)*np.sin(i)*(np.sin(alpha))**2/sqtmp
        dthdA = np.cos(i)/(np.cos(alpha))**2/(1+(np.cos(i)*np.tan(alpha))**2)
        dthdi = -np.sin(i)*np.tan(alpha)/(1+(np.cos(i)*np.tan(alpha))**2)
        zeros = np.zeros(len(jds),dtype=float)
        ones = zeros+1.0
        drho =  [[(drhodA+drhodnu)*dAdt], [(drhodA+drhodnu)*dAdp],  [rho/a], [drhode+(drhodA+drhodnu)*dAde], [zeros], [drhodA*np.pi/180.], [drhodi*np.pi/180.]]
        dth  =  [[dthdA*dAdt], [dthdA*dAdp], [zeros], [dthdA*dAde], [ones*np.pi/180.], [dthdA*np.pi/180.], [dthdi*np.pi/180.]]*180/np.pi
        outderiv =  [[[drho]], [[dth]]]
    else: outderiv=-1.0
    
    projfact = np.cos(alpha)**2 # + e*cos(w)
    vr = np.cos(alpha) + e*np.cos(w)

        
    return rho,theta*180.0/np.pi,vr,projfact,outderiv
 
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    
    