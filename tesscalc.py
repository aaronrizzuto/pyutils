import numpy as np

def prec(imag,cadence='s'):
    '''
    calculate tess precisions based on input Imag


    '''
    ##we know these numbers so far from TESS documents for 1 hour of exposure
    data_i = [10.0,11.0,12.0,13.0,14.0,15.0,16.0,17.0,18.0]##I-magnitude    
    data_p = [0.2 ,0.35,0.6 ,1.1 ,2.3 ,5.0 ,10.0,32.0,70.0] ##millimag
    nfloor = 0.06 ##mmag noise floor
    
    if (cadence != 'l') & (cadence != 's'): 
        print 'Using Long Cadence (30 mins)'
        print 'Option are s=2min, l=30min'
        cadence = 'l'
    if cadence == 's': cadencefactor = np.sqrt(30.0)
    if cadence == 'l': cadencefactor = np.sqrt(2.0)

    
    ##if outside limits scale by flux
    if imag <= data_i[0]: precision = data_p[0]/np.sqrt(10**((data_i[0]-imag)/2.5))
    if imag >= data_i[-1]: precision = data_p[-1]*np.sqrt(10**((imag-data_i[-1])/2.5))
    ##otherwise interpolate
    if (imag > data_i[0]) & (imag < data_i[-1]): precision = np.interp(imag,data_i,data_p)
    
    ##adjust for cadence
    precision *= cadencefactor
    if precision < nfloor: precision = nfloor*1.0
   
    return precision
    
