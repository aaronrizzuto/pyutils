# README #
### What is this repository for? ###

* Basic astronomy utilities for python, many based on IDL astrolib but more python-styled.
* Also lots of nice functions for dealing with common ETL scenarios in Astro (e.g. making tex tables and loading ascii and csv data).
* Also a matplotlib class for cursor interactions.
* Lots of these functions are used for other repositories.

### How do I get set up? ###

* Just clone
* Only dependencies are numpy, matplotlib
### Who do I talk to? ###

* Aaron Rizzuto