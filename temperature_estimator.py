import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pdb,os,sys
import mpyfit


#We need to estimate temperature based on a model temperature scale, and input colors.
#two ways to do this: 
#(1)just interpolate off an isochrone or table. (Simple but can fail for certain cases)
#(2) fit all colors to an isochrone. (complicated but can overcome e.g. reddening)

def color_fitfunc(p,args):
    ##p = logTe, B-V
    ##unpack args
    col,sig_col,ctab,redvals = args    
    usectab = ctab.copy()
    ##redden each color appropriately
    usectab[:,0]+=redvals[0]*p[1]
    usectab[:,1]+=redvals[1]*p[1]
    usectab[:,2]+=redvals[2]*p[1]
    
    ##interpolate:
    modc1 = np.interp(p[0],usectab[:,3],usectab[:,0])
    modc2 = np.interp(p[0],usectab[:,3],usectab[:,1])
    modc3 = np.interp(p[0],usectab[:,3],usectab[:,2])
    
    resid = (col - [modc1,modc2,modc3])/sig_col
    return resid

    

def temperature_estimator(cl,sig_cl,mod_cl,redvals):
    initp = np.array([3.7,0.5])
    test = color_fitfunc(initp,(cl[0],sig_cl[0],mod_cl,redvals))
    
    results = np.zeros((len(cl),2))
    chi2    = np.zeros(len(cl))
    sresults= np.zeros((len(cl),2))
   # pdb.set_trace()
    parinfo2 = [{'fixed':False, 'limits':(None,None)} for dddd in range(initp.shape[0])]
    parinfo2[0]['limits']    = (np.min(mod_cl[:,3]),np.max(mod_cl[:,3]))
    parinfo2[1]['limits']    = (0.0,10.0)
    for i in range(0,len(cl)):
       
        print 'colorfitting ' + str(i+1) + ' out of ' + str(len(cl))        
        colfit,fitinfo = mpyfit.fit(color_fitfunc,initp,args=(cl[i],sig_cl[i],mod_cl,redvals),parinfo=parinfo2,maxiter=50) ##args = t,fl,sig_fl,s,ttime        
        results[i] = colfit*1.0 
        chi2[i] = fitinfo['bestnorm']*1.0
        sresults[i] = fitinfo['parerrors']*1.0
       
    return results,chi2,sresults
        
    #pdb.set_trace()











