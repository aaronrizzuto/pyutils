##code to turn XYZUVW + covariance matrix into ra+dec+distance+pmra,pmdec,RV
##we will always assume XYZ has no covariance with UVW
##standard stuff
import numpy as np
import pdb
import matplotlib.pyplot as plt
from ACRastro.gal_uvw import gal_uvw

##my codes
from uvwtopm import uvwtopm
from gal_xyz import gal_xyz
from ACRastro.glactc import glactc

##version with no covarianve between position and velocity
def gal_to_obs(xyz,cxyz,uvw,cuvw):
	##define sample number 
	#vars_s = np.diag(cxyz)
	#vars_v = np.diag(cuvw)
	#nsamps_s = np.prod(1.96**2*vars_s)
	#nsamps_v = np.prod(1.96**2*vars_v)
	nsamps_s = 10000
	nsamps_v = 10000
	##randomly sample from the multivariate gaussian of N(posvel,cposvel)
	r_s = np.random.multivariate_normal(xyz,cxyz,int(nsamps_s))
	r_v = np.random.multivariate_normal(uvw,cuvw,int(nsamps_v))

	rl,rb,rplx = gal_xyz(r_s[:,0],r_s[:,1],r_s[:,2],reverse=True,plx=True)
	r_ra,r_dec = glactc(rl*180/np.pi,rb*180/np.pi,2000,degree=True,reverse=True)
	r_pos = np.transpose(np.array([r_ra*np.pi/180,r_dec*np.pi/180,rplx]))
	##pdb.set_trace()

	pmvect = uvwtopm(r_v,r_ra*np.pi/180,r_dec*np.pi/180,rplx)
	##reorder outputs
	obsv = pmvect.copy()
	obsv[:,0] = pmvect[:,1]
	obsv[:,1] = pmvect[:,2]
	obsv[:,2] = pmvect[:,0]

	##calculate covariance matrices for 
	cov_v = np.cov(obsv,rowvar=0)
	cov_s = np.cov(r_pos,rowvar=0)
	
	mean_v = np.mean(obsv,axis=0)
	mean_s = np.mean(r_pos,axis=0)
	
	#pdb.set_trace()
	return mean_s,mean_v,cov_s,cov_v
	
	
#full 6D version
def gal_to_obs_full(posvel,cposvel,nsamps=10000):

	rposvel = np.random.multivariate_normal(posvel,cposvel,nsamps)
	#turn random XYZ values into ra,dec,plx

	reqvel = rposvel.copy()
	rl,rb,rplx = gal_xyz(reqvel[:,0],reqvel[:,1],reqvel[:,2],reverse=True,plx=True)
	r_ra,r_dec = glactc(rl*180/np.pi,rb*180/np.pi,2000,degree=True,reverse=True)
	reqvel[:,0] = np.mod(r_ra +360.0,360.0,)
	reqvel[:,1] = r_dec
	reqvel[:,2] = rplx
	
	##now turn random UVW velocities into pmra,pmdec,RV
	pmvect = uvwtopm(rposvel[:,3:],r_ra*np.pi/180,r_dec*np.pi/180,rplx)
	
	reqvel[:,3] = pmvect[:,1]
	reqvel[:,4] = pmvect[:,2]	
	reqvel[:,5] = pmvect[:,0]
	
	##make covariance matrix
	cov_full = np.cov(reqvel,rowvar=0)
	
	##make means
	means_full = np.mean(reqvel,axis=0)
	
	##pdb.set_trace()
	return means_full,cov_full
	
def obs_to_gal_full(pospm,cpospm,nsamps=10000):
    
    rpospm = np.random.multivariate_normal(pospm,cpospm,nsamps)
    
    #turn random ra, dec ,plx into xyz numbers
    rx,ry,rz =gal_xyz(rpospm[:,0],rpospm[:,1],rpospm[:,2],radec=True,plx=True,reverse=False)
    
    ##now calculate the UVW numbers 
    ru,rv,rw  = gal_uvw(ra=rpospm[:,0], dec=rpospm[:,1], pmra=rpospm[:,3], pmdec=rpospm[:,4], vrad=rpospm[:,5], plx=rpospm[:,2])
    rgal = rpospm.copy()
    rgal[:,0] = rx 
    rgal[:,1] = ry
    rgal[:,2] = rz
    rgal[:,3] = ru
    rgal[:,4] = rv
    rgal[:,5] = rw

    means_full = np.mean(rgal,axis=0)
    cov_full = np.cov(rgal,rowvar=0)
    return means_full, cov_full

def obs_to_gal_sample(pospm,cpospm,nsamps=1000):
    ##as with the above method, but returns the samples instead of the constructed cov_mat/means
    rpospm = np.random.multivariate_normal(pospm,cpospm,nsamps)
    
    #turn random ra, dec ,plx into xyz numbers
    rx,ry,rz =gal_xyz(rpospm[:,0],rpospm[:,1],rpospm[:,2],radec=True,plx=True,reverse=False)
    
    ##now calculate the UVW numbers 
    ru,rv,rw  = gal_uvw(ra=rpospm[:,0], dec=rpospm[:,1], pmra=rpospm[:,3], pmdec=rpospm[:,4], vrad=rpospm[:,5], plx=rpospm[:,2])
    rgal = rpospm.copy()
    rgal[:,0] = rx 
    rgal[:,1] = ry
    rgal[:,2] = rz
    rgal[:,3] = ru
    rgal[:,4] = rv
    rgal[:,5] = rw

    return rgal

def vel_to_obs(ra,dec,plx,uvw,cuvw,nsamps = 1000):

	##randomly sample from the multivariate gaussian of N(posvel,cposvel)
	r_v = np.random.multivariate_normal(uvw,cuvw,int(nsamps))

	
	
	
	##pdb.set_trace()
	rplx   = np.zeros(nsamps)+plx
	r_dec  = np.zeros(nsamps)+dec
	r_ra   = np.zeros(nsamps)+ra
	pmvect = uvwtopm(r_v,r_ra*np.pi/180,r_dec*np.pi/180,rplx)
	##reorder outputs
	obsv = pmvect.copy()
	obsv[:,0] = pmvect[:,1]
	obsv[:,1] = pmvect[:,2]
	obsv[:,2] = pmvect[:,0]

	##calculate covariance matrices for 
	cov_v = np.cov(obsv,rowvar=0)
	
	mean_v = np.mean(obsv,axis=0)
	
	#pdb.set_trace()
	return mean_v,cov_v