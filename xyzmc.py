import numpy as np
import pdb
from  gal_xyz import gal_xyz

def xyzmc(ra,dec,plx,sig_ra,sig_dec,sig_plx,nsamp=10000,plxtype=True):
    rplx  =np.random.normal(loc=plx, scale=sig_plx, size=nsamp)
    rra =np.random.normal(loc=ra, scale=sig_ra, size=nsamp)    
    rde =np.random.normal(loc=dec, scale=sig_dec, size=nsamp)
 
    

    xx = np.zeros(nsamp)
    yy = np.zeros(nsamp)
    zz = np.zeros(nsamp)
    for i in range(nsamp):xx[i],yy[i],zz[i] = gal_xyz(rra[i],rde[i],rplx[i],radec=True,plx=plxtype)
    return np.mean(xx),np.mean(yy),np.mean(zz),np.std(xx),np.std(yy),np.std(zz)