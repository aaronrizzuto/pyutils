import numpy as np
import bottleneck as bn

#@profile
def robustmean(y,cut,usebn=True):
    if usebn==True: 
        mfunc = np.mean
        medfunc = bn.median
    else: 
        medfunc=np.median
        mfunc  = np.mean
    absdev    = np.absolute(y-medfunc(y))
    medabsdev = medfunc(absdev)/0.6745
    if medabsdev < 1.0e-24: medabsdev = mfunc(absdev)/0.8
    gind = np.where(absdev <= cut*medabsdev)[0]
    sigma    = bn.nanstd(y[gind])
    sc       = bn.nanmax([cut,1.0])
    
    if sc <= 4.5: sigma = sigma/(-0.15405+0.90723*sc-0.23584*sc**2+0.020142*sc**3) 
    goodind  = np.where(absdev <= cut*sigma)[0]
    #goodpts  = y[goodind]
    sigma    = bn.nanstd(y[goodind])
    #sigma    = np.std(y[goodind])
    
    
    
    sc = bn.nanmax([cut,1.0])
    if sc <= 4.5: sigma=sigma/(-0.15405+0.90723*sc-0.23584*sc**2+0.020142*sc**3) 
    
         
    return mfunc(y[goodind]),sigma/np.sqrt(len(y)-1.0),len(y) - len(goodind),goodind
  