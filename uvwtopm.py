## Function to turn UVW velocity to PM/Vrad
## Takes RA, Dec, Parallax as inputs -- scalar and array

import numpy as np
import pdb
#import time

def uvwtopm(vel,ra,dec,plx):
    '''Function to compute PM/Vrad from a given UVW velocity (in km/s)
    RA/Dec inputs in radians
    Plx input in milliarcseconds
    
    Returns array with entries RV, PM_RA, PM_Dec'''
                  
    k = 4.74047     # Equivalent of 1 A.U/yr in km/s
    
#	start = time.clock()
#	#start with the T matrix
    
    radeg = np.pi/180.0
    tet0  = 123.0 * radeg
    angp  = 192.25 * radeg
    dngp  = 27.4 * radeg
    cos   = np.cos
    sin   = np.sin
    T1    = np.matrix([[cos(tet0),sin(tet0),0.0],[sin(tet0),-cos(tet0),0.0],[0.0,0.0,1.0]])
    T2    = np.matrix([[-sin(dngp),0.0,cos(dngp)],[0.0,-1.0,0.0],[cos(dngp),0.0,sin(dngp)]])
    T3    = np.matrix([[cos(angp),sin(angp),0.0,],[sin(angp),-cos(angp),0.0],[0.0,0.0,1.0]])
    Tmat  = T1 * T2 * T3
    
    Tmat[0,:] = -1.0 * Tmat[0,:]
    
    invT  = Tmat.I
    
#	#we now need to build a coord matrix for each star then do a matrix multiplication
#	#and inverse. This is fairly slow unfortunately.......
    
    pmvect = vel * 0.0
    
#	#start = time.clock()
#	#this loop is super slow, need a better method
#	for i in range(0,len(ra)):
#		Amat = np.matrix([[cos(ra[i])*cos(dec[i]),-sin(ra[i]),-cos(ra[i])*sin(dec[i])],
#			[sin(ra[i])*cos(dec[i]),cos(ra[i]),-sin(ra[i])*sin(dec[i])],
#			[sin(dec[i]),0.0,cos(dec[i])]])
#		pdb.set_trace()
#		Bmat = Tmat*Amat
#		invB = Bmat.I
#		ttt = np.array([vel[i,:]])
#		#ppp = np.array(invB*ttt.T)[:,0]
#		pmvect[i,:] =  np.array(invB*ttt.T)[:,0]*np.array([1.0,plx[i],plx[i]])
#	
#	#try this a more direct way by calculating everything by hand, not detA=1.0 always:
#	#Amat = np.array([[cos(ra)*cos(dec),-sin(ra),-cos(ra)*sin(dec)],[sin(ra)*cos(dec),cos(ra),-sin(ra)*sin(dec)],[sin(dec),dec*0.0,cos(dec)]])
    
    invA = np.array([[cos(ra)*cos(dec),sin(ra)*cos(dec),sin(dec)],	[-sin(ra),cos(ra),dec*0.0],	[-cos(ra)*sin(dec),-sin(ra)*sin(dec),cos(dec)]])
    
    
    if not np.isscalar(ra):
        # Computation for an array input
        invA   = np.transpose( invA, (2,0,1) )
        invTA  = np.dot( invA, np.array(invT) )
        pmvect = np.dot( invTA, vel ) * np.array( [ plx * 0.0 + 1, plx, plx ] ).T
        pmvect[:,1:3] /= k
    else:
        # Computation for a scalar input
        invTA  = np.dot( invA, np.array( invT ) )
        pmvect = np.dot( invTA, vel ) * np.array( [ 1.0, plx, plx ] )
        pmvect[1:3] = pmvect[1:3]/k
        
    return pmvect.T

#	pdb.set_trace()
#	invA  = np.transpose(invA,(2,0,1))
#	#now multiply with Tmat inverse invT (want Ainv * Tinv really)
#	invTA = np.dot(invA,np.array(invT))
#	if len(vel.shape) > 1:
#		invA  = np.transpose(invA,(2,0,1))
#		invTA = np.dot(invA,np.array(invT))
#		#pdb.set_trace()
#		newvel=  np.transpose(np.tile(vel[:,:,np.newaxis],(1,1,3)),(0,2,1))
#		pmvect = np.sum(invTA*newvel,axis=2)*np.transpose(np.array([plx*0.0+1.0,plx,plx]))
#		##pdb.set_trace()
#		pmvect[:,1:3] = pmvect[:,1:3]/k
#		##pdb.set_trace()
#	if len(vel.shape) == 1:
#		invTA = np.dot(invA,np.array(invT))
#		##pdb.set_trace()
#		pmvect = np.dot(invTA,vel)*np.array([1.0,plx,plx])
#		pmvect[1:3] = pmvect[1:3]/k
#	return pmvect