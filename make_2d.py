import numpy as np
import rebin

def make_2d(x,y):
    ny = len(y)
    nx = len(x)
    xx = x.reshape(1,nx)
    yy = y.reshape(ny,1)

    xx = rebin.rebin(xx, [ny, nx])
    yy = rebin.rebin(yy, [ny, nx])

    return(xx,yy)