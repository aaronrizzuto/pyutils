import numpy as np
def mas2rad(mas):
    return mas/1000.0/3600*np.pi/180

def rad2mas(rad):
    return rad*180/np.pi*3600.*1000.