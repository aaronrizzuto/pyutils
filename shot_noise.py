import numpy as np
import emcee

##k is the observed number of occurences and we want to know what lambda might be
##using bayes theorem


def pmod(p,k=None,jef=False):
    lam=p[0]
    if lam < 0.0000000000001: return -np.inf

    pdm = lam**k*(np.exp(-lam))/np.math.factorial(k)
    if jef == True: pdm*= np.sqrt(1/lam)
    return np.log(pdm)




def sample(k,jef=True,sampnum=10000):

##if k ge 1000: return [np.sqrt(k),np.sqrt(k)]

    if k > 120: 
        print('returning standard sqrt range')
        return [100.0,np.sqrt(10),np.sqrt(10)]
    nwalkers=10
    ndim = 1
    nsamps=sampnum
    sig_pstart = np.random.uniform(np.max([0.00001,k-3*np.sqrt(k)]),3*np.sqrt(k)+k,nwalkers)
    p0 = [[ddd] for ddd in sig_pstart]
    sampler = emcee.EnsembleSampler(nwalkers,ndim,pmod,args=[k,jef])
    ppos,pprob,pstate = sampler.run_mcmc(p0,nsamps,progress=True)
    burn=int(0.5*nsamps)
    samples = sampler.chain[:,burn:,:].reshape((-1,ndim))
    result = np.zeros((ndim,3),float)
    for i in range(ndim): result[i,:]=np.percentile(samples[:,i],[16,50,84])
    bestmod=result[:,1]
    sig_lower = result[:,1]-result[:,0]
    sig_upper=  result[:,2]-result[:,1]
    import pdb
    pdb.set_trace()
    return [bestmod[0],sig_lower[0],sig_upper[0]]
    
    
    

