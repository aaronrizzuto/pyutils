import numpy as np

def circline(r,cen):

    theta = np.linspace(0,360,360*3,endpoint=True)
    x = r*np.cos(theta*180/np.pi)+cen[0]
    y = r*np.sin(theta*180/np.pi)+cen[1]
    
    return x,y