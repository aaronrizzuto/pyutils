import numpy as np
def robustmean(y,cut):     
    absdev    = np.absolute(y-np.median(y))
    medabsdev = np.median(absdev)/0.6745
    if medabsdev < 1.0e-24: medabsdev = np.mean(absdev)/0.8
    gind = np.where(absdev <= cut*medabsdev)[0]
    sigma    = np.std(y[gind])
    sc       = np.max([cut,1.0])
    
    if sc <= 4.5: sigma = sigma/(-0.15405+0.90723*sc-0.23584*sc**2+0.020142*sc**3) 
    goodind  = np.where(absdev <= cut*sigma)[0]
    #goodpts  = y[goodind]
    sigma    = np.std(y[goodind])
    
    sc = np.max([cut,1.0])
    if sc <= 4.5: sigma=sigma/(-0.15405+0.90723*sc-0.23584*sc**2+0.020142*sc**3)      
    return np.mean(y[goodind]),sigma/np.sqrt(len(y)-1.0),len(y) - len(goodind),goodind