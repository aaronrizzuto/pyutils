import numpy as np
import jdtools,datetime
import pdb

def ephems(t0,per,tdur,sig_t0=0.0,sig_per=0.0,sig_tdur=0.0,tess=False,giveme=10,edgetime=0,starttime=-1):
    '''converts t0, period in JD into a list of UT datetime times.
        everything input should be in units of days, with tdur being the start of ingress to end of 
        egress time in days.
        edgetime is in hours
    '''
    
    if tess == True: t0 = jdtools.tess_to_jd(t0)
    
    ##current time
    ctime = datetime.datetime.utcnow()
    ##convert to jd:
    jdnow = jdtools.date_to_jd(ctime.year,ctime.month,ctime.day+jdtools.hmsm_to_days(hour=ctime.hour,min=ctime.minute,sec=ctime.second))
    if starttime>2400000: jdnow=starttime
    ##now we want to find the next X transits
    

    ttimes = np.zeros(100001)
    ttimes[0] = t0
    sig_ttimes = ttimes*0.0
    sig_ttimes[0] = sig_t0
    for i in range(100000):
        ttimes[i+1] = t0+per*(i+1)
        sig_ttimes[i+1] = sig_t0 + sig_per*(i+1)
        
    future = np.where(ttimes >= jdnow)[0]
    ttimes = ttimes[future][0:giveme]
    sig_ttimes = sig_ttimes[future][0:giveme]
    
            
    itimes = ttimes-tdur/2 - edgetime/24.0
    etimes = ttimes+tdur/2 + edgetime/24.0
    ##convert timing uncertainties to minutes and add duration error
    sig_itimes = (sig_ttimes+sig_tdur/2)*24.0*60.0
    sig_etimes = (sig_ttimes+sig_tdur/2)*24.0*60.0
    sig_ttimes*=24.0*60.0
    ##these are all in JD, convert to UT date/times.
    ##
    print('HERE ARE THE NEXT ' + str(giveme) + ' TRANSIT TIMES!!!')
    for i in range(len(ttimes)):
        y,mn,d,h,m,s,ms = jdtools.jd_to_date(ttimes[i],subdays=True)
        yi,mni,di,hi,mi,si,msi = jdtools.jd_to_date(itimes[i],subdays=True)
        ye,mne,de,he,me,se,mse = jdtools.jd_to_date(etimes[i],subdays=True)
        print('Transit ' + str(i+1))
        print('INGRES (UT): ' + str(yi)+'-'+str(mni).zfill(2)+'-'+str(di).zfill(2)+'|'+str(hi).zfill(2)+':'+str(mi).zfill(2)+':'+str(si).zfill(2) + ' +-'+str(int(np.ceil(sig_itimes[i])))+'mins |' + str(itimes[i]))     
        print('EGRES  (UT): ' + str(ye)+'-'+str(mne).zfill(2)+'-'+str(de).zfill(2)+'|'+str(he).zfill(2)+':'+str(me).zfill(2)+':'+str(se).zfill(2) + ' +-'+str(int(np.ceil(sig_itimes[i])))+'mins |' + str(etimes[i]))
        print('-------------------------------------------------------------')
        #pdb.set_trace()