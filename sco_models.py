import numpy as np 
import pdb


def chen_model(gnum):
    if gnum == 1: uvw = np.array([6.4,-15.9,-7.4])
    if gnum == 2: uvw = np.array([5.1,-19.7,-4.6])    
    if gnum == 3: uvw = np.array([7.8,-20.7,-6.0])
    sig_int=3.0
    return uvw,sig_int



def rizzuto_model(gl):


    bparams    = np.array([ 0.20952391 ,-54.984992] )
    bsigparams = np.array([-0.097688882, 45.612664]  )
    distparams = np.array([0.0058992004,-0.0072425234])
    sig_rg_bar  = 25.0

    bg = bparams[0]*gl+bparams[1]
    bsigg = bsigparams[0]*gl+bsigparams[1]
  #  pdb.set_trace()
    rg_bar  = (distparams[0]*np.cos(gl*np.pi/180)+ distparams[1]*np.sin(gl*np.pi/180))**(-1)


    uvw_a = np.array([-0.13389093, 0.0089457638, 0.019123222])
    uvw_b = np.array([ 50.863992 ,-20.810681   ,-12.262665])
    uvw   = uvw_a*gl+uvw_b
    sig_int = 3.0
    return uvw,sig_int,rg_bar,sig_rg_bar
    


    