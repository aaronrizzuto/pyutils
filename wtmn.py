import numpy as np
import pdb



def sig_wtmn(x,sig_x):
    '''
    calculate variance weighted mean, and unbiased actual variance estimator
       
    '''
    weights = 1/sig_x**2
    V1 = np.sum(weights)
    V2 = np.sum(weights**2) 
    mu  = np.sum(x*weights)/V1
    var = np.sum(weights*(mu-x)**2)/(V1-V2/V1)
    return mu,np.sqrt(var)
    
    
    
def wtmn(x,sigma):
    weights = 1/sigma**2
    nelt = len(x)*1.0
    sumwt = np.sum(weights)
    mean = np.sum(x*weights)/sumwt
    e1 = 1.0/sumwt
    if nelt > 1:
        MSE = np.sum(np.absolute(x-mean)**2*weights)/(nelt-1)
    else: MSE = 1.0
    e2  = MSE*1.0
    e2  = e2/sumwt
    var = (2*e1 + (nelt-1)*e2)/(nelt+1.0)
    sdev = np.sqrt(var)
    
    wts = weights/sumwt*np.sqrt(var/e1)
    
    
    
    return mean,sdev,wts
    
    