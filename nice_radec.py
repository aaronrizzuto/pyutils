import numpy as np
import pdb
def nice_radec(rah,ram,ras,ded,dem,des,spc=' ',mspc=' '):
    output = ('',)
    for i in range(0,len(rah)):
        rh = str(rah[i])
        rm = str(ram[i])
        rs = str(np.round(ras[i],decimals=2))	
        dd = str(ded[i])
        dm = str(dem[i])
        ds = str(np.round(des[i],decimals=1))
		
		## add zeros/spaces to the ra
        if len(rh) < 2: rh='0'+rh
        if len(rm) < 2: rm='0'+rm
        ttt = rs.split('.')[1]
        if len(ttt) < 2: rs = rs + '0'
        if len(rs) < 5: rs='0'+rs
		##and same for dec, but deal with sign
        sdd='+'
        if dd[0] == '-':
            sdd = '-'
            dd=dd[1:]
        if len(dd) < 2: dd='0'+dd
        if len(dm) < 2: dm='0'+dm
        if len(ds) < 4: ds='0'+ds
        #ttt = ds.split('.')[1]
        #if len(ttt) < 2: ds = ds + '0'

        dd = sdd+dd
        output = output+(rh+spc+rm+spc+rs+mspc+dd+spc+dm+spc+ds,)
    return output[1:]
    
def wrap_nice_radec(ra,dec,spc=' ',mspc=' '):
    from ACRastro.radec import radec
    rah,ram,ras,ded,dem,des = radec(ra,dec)
    return nice_radec(rah,ram,ras,ded,dem,des,spc=spc,mspc=mspc)
    
    