'''
A bunch of functions (added to over time) 
to do simple membership things, like HRD tests,
or calculate photometric distances

'''

import numpy as np
import os,sys,pdb


def main_seq_test(col,mag,modcol,modmag,distance,buffer=0.0,plotit=False):
    '''
    Identify things below the main sequence at a particular distance
    col = numpy array of colors
    mag = numpy array of magnitudes
    modcol = model colors for the main sequence
    modmag = model mags for the main sequence
    distance = distance for the test
    
    Note the test has to be done in the reversed space (Mag, Col) to avoid interpolation issues
    
    output is a numpy array of integers, 1=passed, -1=fail, 
    '''
    from scipy import interpolate
    modmag = modmag -5+5*np.log10(distance)
    interpolator = interpolate.interp1d(modmag+buffer,modcol,fill_value=(-1.0,modcol[0]),bounds_error=False)
    interpcol    = interpolator(mag)
    result = np.zeros(len(mag))-1
    qwe = np.where(interpcol - col <= 0.0)[0]
    result[qwe] = 1
    if plotit == True:
        import matplotlib.pyplot as plt
        plt.plot(col,mag,'.')
        plt.ylim([np.nanmax(mag),np.nanmin(mag)])
        plt.plot(modcol,modmag,'k')
        bad = np.where(result == -1)[0]
        plt.plot(col[bad],mag[bad],'.r')
        plt.show()
    return result
    
    
def dphot(col,mag,modcol,modmag,binbias=0.2,error=0.3,diagnose=False):
    '''
    calculate a photometric distance from an isochrone model
    
    col = numpy array of star colours
    mag = corresponding mags
    modcol= the same color from an isochrone that you want to use
    modmag= magnitudes as above
    
    returns
    pdist     = numpy arrays of photometric distances
    sig_pdist = corresponding uncertainties 
    '''
    from scipy import interpolate
    interpolator = interpolate.interp1d(modcol,modmag,fill_value=(np.min(modmag),np.max(modmag)),bounds_error=False)
    interpmag    = interpolator(col)
    if diagnose==True:
        import matplotlib.pyplot as plt
        import pdb
        pdb.set_trace()
    distance     = 10.0**((mag+binbias-interpmag)/5.0+1.0)
    sig_distance = distance*np.log(10.0)/5.0*error
    return distance,sig_distance
    

    
    
    
    
    
    