import numpy as np


def make_outline(varlist,spacer = ',',replacenans = '-99.99'):
    line = ''
    for var in varlist:
        if np.isnan(var) == True: var = replacenans
        line += str(var)+spacer
    line += ' \n'
    return line



