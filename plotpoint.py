import matplotlib.pyplot as plt
import matplotlib.image as mpimg


##A class for handling listening for user clicks on matplotlib plots and passing the 
##coordinates for use to the user. Kind of like IDL cursor procedure
##doesn't use anything other than matplotlib
class plotpoint():
    def __init__(self,xx,yy):
        self.xx = xx
        self.yy = yy
        self.clickpoints = []
        
    def getcoord(self):
        fig=plt.figure(figsize=(20,12))
        ax = fig.add_subplot(111)
        lll = 'b.'
        if (len(self.xx) > 5000): lll = ',k'
        ax.plot(self.xx,self.yy,lll)
        cid = fig.canvas.mpl_connect('button_press_event', self.__onclick__)
        plt.show()
        return self.clickpoints

    def __onclick__(self,click):
        point = [click.xdata,click.ydata]
        print click.xdata,click.ydata
        return self.clickpoints.append(point)
