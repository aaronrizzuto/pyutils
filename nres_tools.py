import numpy as np
import os,pdb,sys,glob,shutil
from astropy.io import fits
import tarfile

def tarhandle(tarfilename,justheader=False,extract_data=True):
    thistar = tarfile.open(tarfilename)
    mnum = ()
    for j,mem in enumerate(thistar.getmembers()): 
        if mem.name.split('.')[-1] == 'fz': mnum+=(j,)
    if len(mnum) == 1: 
        wnum = mnum
        bnum = mnum 
    if len(mnum) == 0: print('Problem with finding the right tarfile part!')
    if len(mnum)  > 1: ##this means its the old style where the extensions are all seperate files
        basename  = thistar.getmembers()[0].name
        basename = basename+'/'+basename
        wavename = basename+'-wave.fits.fz'
        mainname = basename+'.fits.fz'
        blazename = basename+'-blaze.fits.fz'
        wnum=()
        mnum=()
        bnum=()
        for nnn,fff in enumerate(thistar.getmembers()):
            if fff.name == wavename: wnum+=(nnn,)
            if fff.name == mainname: mnum+=(nnn,)
            if fff.name == blazename: bnum+=(nnn,)
        if (len(wnum) != 1) | (len(mnum) != 1):
            print('fits identifier problem, help!')
            pdb.set_trace()
            
    ## now extract them and return the extracted thing.
       
    mextract = thistar.extractfile(thistar.getmembers()[mnum[0]])
    mainhdu = fits.open(mextract)
    if justheader == True: 
        if mnum==wnum: 
            thehead = mainhdu[0].header
        else: thehead=mainhdu[1].header
        mainhdu.close()
        thistar.close()
        return thehead
    
    if justheader == False:

        if wnum==mnum: 
            wavehdu = mainhdu
            blazehdu = mainhdu
            thehead = mainhdu[0].header
        else:
            wextract = thistar.extractfile(thistar.getmembers()[wnum[0]])
            bextract = thistar.extractfile(thistar.getmembers()[bnum[0]])
            wavehdu = fits.open(wextract)
            blazehdu = fits.open(bextract)
            thehead = mainhdu[1].header
    
    if extract_data == True: ##just return wavelength/flux arrays and header
        
        if wnum == mnum:
            wavedata = mainhdu[6].data.copy()
            wavehead = mainhdu[6].header
            blazedata = mainhdu[3].data.copy()
            blazehead = mainhdu[3].header   
            fluxdata = mainhdu[2].data.copy()
            fluxhead = mainhdu[2].header
    
        else:

            wavedata = wavehdu[1].data.copy()[0]
            fluxdata=  mainhdu[1].data.copy()[0]
            blazedata = blazehdu[1].data.copy()[0]
            blazehead = blazehdu[1].header.copy()
            wavehead=  wavehdu[1].header
            fluxhead=  mainhdu[1].header
        mainhdu.close()
        wavehdu.close()
        blazehdu.close()
        thistar.close()
        return thehead,wavehead,wavedata,fluxhead,fluxdata,blazehead,blazedata
        
    return thehead,mainhdu,wavehdu,blazehdu,thistar