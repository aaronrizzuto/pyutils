import numpy as np

def twoside_normal(loc=0.0,scale1=1.0,scale2=1.0,size=1000):
    
    usize = size*5
    
    n1 = np.random.normal(loc=loc,scale=scale1,size=usize)
    n2 = np.random.normal(loc=loc,scale=scale2,size=usize)
    
    ranger1 = np.where(n1 < loc)[0]
    ranger2 = np.where(n2 >=loc)[0]
    sn = np.min([len(ranger1),len(ranger2)])
    n1=  n1[ranger1[0:sn]]
    n2=  n2[ranger2[0:sn]]
    
    rando = np.concatenate((n1,n2))
    
    qwe = np.random.randint(0,high=len(rando),size=size)
    
    
    return rando[qwe]
    
    