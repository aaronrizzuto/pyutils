#cardelli,clayton and mathis 1989 law.
##currently just the IR and Optical parts, not the UV/Far UV curves


import numpy as np

def law(lam,R=3.1):
  #  print 'here'
  #  pdb.set_trace()
    x = 1.0/lam
    y = x-1.82
    qwe = np.where(x <1.1)[0]
    qwe2 = np.where(x>=1.1)[0]
    ##NIR and OPTICAL
    ax = 1 + 0.17699*y -0.50446*y**2 -0.02427*y**3 +0.7208*y**4+0.01979*y**5 -0.77530*y**6 +0.032999*y**7
    bx = 1.41338*y+2.28305*y**2+1.07233*y**3  - 5.38434*y**4 -0.62251*y**5 +5.30260*y**6 -2.09002*y**7    
    output = np.zeros(len(lam))
    output[qwe2] = ax[qwe2]+bx[qwe2]/R
    
    ##IR
    ax = 0.574*x**(1.61)
    bx = -0.527*x**(1.61)
    output[qwe] = ax[qwe]+bx[qwe]/R

    return output
    
