import pdb
import numpy as np

def smooth(x,window_len,win='flat',simsize=True):

##pad the signal:
    s = np.r_[x[window_len-1:0:-1],x,x[-1:-window_len:-1]]

##make up the window
    if not win in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']: 
        print 'I dont known that type of window bro'
        print 'Use: flat, hanning, hamming, bartlett, blackman'
        pdb.set_trace()
    
    if win == 'flat':
        w=np.ones(window_len,'d') ##makes a moving average over this window length
    else:
        w=eval('np.'+win+'(window_len)')



    y=np.convolve(w/w.sum(),s,mode='valid')
    
    if simsize == True:
        y = y[(window_len-1)/2:len(y)-(window_len-1)/2]

    return y