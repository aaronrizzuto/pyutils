import numpy as np

def radec(ra,dec):

	rah = np.zeros(len(ra),dtype=int)
	ram = np.zeros(len(ra),dtype=int)
	ras = np.zeros(len(ra),dtype=float)
	ded = np.zeros(len(ra),dtype=int)
	dem = np.zeros(len(ra),dtype=int)
	des = np.zeros(len(ra),dtype=float)
	ra = ra/15.0
	rah = np.floor(ra)
	newra = ra - rah
	ram = np.floor(newra*60.0)
	newra = newra - ram/60.0
	ras = newra*3600.0
			
	sign_dec = np.absolute(dec)/dec
	
	dec = np.abs(dec)
	ded = np.floor(dec)
	newdec = dec-ded
	ded = ded*sign_dec
	dem = np.floor(newdec*60.0)
	newdec = newdec-dem/60.0
	des = newdec*3600.0
	rah = rah.astype(int)
	ram = ram.astype(int)
	ded = ded.astype(int)
	dem = dem.astype(int)
		
	return rah,ram,ras,ded,dem,des