## something to do IDL jprecess but without fancy things like moving stars with pmra,pmdec,vrad
## assumes np.array inputs

##IMPORTS:
import numpy as np
##import pdb

def jprecess(ra,dec,epoch):
	radeg = 180./np.pi
	sec_to_radian = 1/radeg/3600.0
	
	m =  np.matrix([[+0.9999256782, +0.0111820610, +0.0048579479,
	 -0.000551,     +0.238514,     -0.435623     ], 
	[ -0.0111820611, +0.9999374784, -0.0000271474,   
	-0.238565,     -0.002667,      +0.012254      ], 
	[ -0.0048579477, -0.0000271765, +0.9999881997 , 
	+0.435739,      -0.008541,      +0.002117      ], 
	[ +0.00000242395018, +0.00000002710663, +0.00000001177656, 
	+0.99994704,    +0.01118251,    +0.00485767    ], 
	[ -0.00000002710663, +0.00000242397878, -0.00000000006582, 
	-0.01118251,     +0.99995883,    -0.00002714    ], 
	[ -0.00000001177656, -0.00000000006587, 0.00000242410173, 
	-0.00485767,   -0.00002718,     1.00000956] ])

	a = 1e-6*np.array([ -1.62557, -0.31919, -0.13843])        ##in radians
	a_dot = 1e-3*np.array([1.244, -1.579, -0.660 ])

	#if epoch != 1950:
	a = a + sec_to_radian*a_dot*(epoch-1950.0)/100.0
	ra_rad  = ra/radeg
	dec_rad = dec/radeg
 	cosra  = np.cos(ra_rad)  
 	sinra  = np.sin(ra_rad)
 	cosdec = np.cos(dec_rad)
 	sindec = np.sin(dec_rad)
 	
	ra_2000 = ra*0.0
 	dec_2000 = dec*0.0
 	
 	if hasattr(ra,"__len__") == False:
 		NN = 1
 	if hasattr(ra,"__len__") == True:
		NN = len(ra)
		
 	for i in range(0,NN):
 		r0 = np.array([ cosra[i]*cosdec[i], sinra[i]*cosdec[i], sindec[i] ])
		r1 = r0 - a + np.sum(r0*a)*r0
		r1_dot = -a_dot + np.sum(r0*a_dot)*r0
		R_1 = np.concatenate((r1, r1_dot))
		R = R_1*m
		t = ((epoch - 1950.0) - 50.00021)/100.0
		#pdb.set_trace()
		#rr = [ R[0,0], R[0,1], R[0,2]]
		rr = R[0,0:3]
		v =  R[0,3:]
			#v  = np.arra[ R[0,3], R[0,4], R[0,5]]
		rr1 = rr + sec_to_radian*v*t
		x = rr1[0,0]
		y = rr1[0,1]
		z = rr1[0,2]
		r2 = x**2+y**2+z**2
		dec_2000[i] = np.arcsin(z/np.sqrt(r2))
		ra_2000[i]  = np.arctan2(y,x)
		
	return ra_2000*180./np.pi,dec_2000*180./np.pi