import numpy as np
from numpy import *

def mandelecl(eclparams, t, etc = []):
   """
  This function computes the secondary eclipse shape using equations provided by Mandel & Agol (2002)

  Parameters
  ----------
    midpt:  Center of eclipse
    width:  Eclipse duration from contacts 1 to 4
    depth:  Eclipse depth
    t12:    Eclipse duration from contacts 1 to 2
    t34:    Eclipse duration from contacts 3 to 4
    flux:   Flux offset from 0
    t:        Array of phase points

  Returns
  -------
    This function returns the flux for each point in t.

  Revisions
  ---------
  2008-05-08    Kevin Stevenson, UCF
                kevin218@knights.ucf.edu
                Original version
   """
   #DEFINE PARAMETERS
   midpt, width, depth, t12, t34, flux = eclparams

   #COMPUTE TIME OF CONTACT POINTS
   t1           = midpt - width / 2.
   t2           = min(t1 + t12, midpt)
   t4           = midpt + width / 2.
   t3           = max(t4 - t34, midpt)
   ieclipse     = np.where((t >= t2) & (t <= t3))
   iingress     = np.where((t >  t1) & (t <  t2))
   iegress      = np.where((t >  t3) & (t <  t4))

   p            = np.sqrt(abs(depth))*np.sign(depth)
   y            = np.ones(len(t))
   y[ieclipse]  = 1.0 - depth
   if p != 0:
         #Use Mandel & Agol (2002) for ingress of eclipse
      z           = -2 * p * (t[iingress] - t1) / t12 + 1 + p
      k0          = np.arccos((p**2 + z**2 - 1) / 2 / p / z)
      k1             = np.arccos((1 - p**2 + z**2) / 2 / z)
      y[iingress] = 1.0 - np.sign(depth) / np.pi * (p**2 * k0 + k1          \
                  - np.sqrt((4 * z**2 - (1 + z ** 2 - p**2)**2) / 4))
      #Use Mandel & Agol (2002) for egress of eclipse
      z              = 2 * p * (t[iegress] - t3) / t34 + 1 - p
      k0             = np.arccos((p**2 + z**2 - 1) / 2 / p / z)
      k1             = np.arccos((1 - p**2 + z**2) / 2 / z)
      y[iegress]  = 1.0 - np.sign(depth) / np.pi * (p**2 * k0 + k1          \
                  - np.sqrt((4 * z**2 - (1 + z**2 - p**2)**2) / 4))
   return y*flux

def mandeltr(params, t, etc):
    """
  This function computes the primary transit shape using equations provided by Mandel & Agol (2002)

  Parameters
  ----------
    midpt:  Center of eclipse
    rprs:   Planet radius / stellar radius
    cosi:   Cosine of the inclination
    ars:    Semi-major axis / stellar radius
    flux:   Flux offset from 0
    t:        Array of phase/time points
    p:      Period in same units as t

  Returns
  -------
    This function returns the flux for each point in t.

  Revisions
  ---------
  2010-11-27    Kevin Stevenson, UCF
                kevin218@knights.ucf.edu
                Original version
    """
    #DEFINE PARAMETERS
    midpt, rprs, cosi, ars, flux, p = params

    #COMPUTE z for transit only (not eclipse)
    z  = ars*np.sqrt(np.sin(2*np.pi*(t-midpt)/p)**2 + (cosi*np.cos(2*np.pi*(t-midpt)/p))**2)
    z[np.where(np.bitwise_and((t-midpt)%p > p/4.,(t-midpt)%p < p*3./4))] = ars
    #INGRESS/EGRESS INDICES
    iingress = np.where(np.bitwise_and((1-rprs) < z, z <= (1+rprs)))[0]
    #COMPUTE k0 & k1
    k0 = np.arccos((rprs**2 + z[iingress]**2 - 1) / 2 / rprs / z[iingress])
    k1 = np.arccos((1 - rprs**2 + z[iingress]**2) / 2 / z[iingress])

    #CALCULATE TRANSIT SHAPE
    y = np.ones(len(t))
    y[np.where(z <= (1-rprs))] = 1.-rprs**2
    y[iingress] = 1. - 1./np.pi*(k0*rprs**2 + k1 - np.sqrt((4*z[iingress]**2 - \
                  (1 + z[iingress]**2 - rprs**2)**2)/4))

    return y*flux