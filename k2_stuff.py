import numpy as np
import pdb

def kpm_jhk(j,k):
    x=j-k
    kpmag = 0.42443603 + 3.7937617*x - 2.3267277*x**2 + 1.4602553*x**3 + k
    return kpmag
    

def kpm_gr(g,r):
    x=g-r
    kpmag = 0.2*g+0.8*r
    qwe = np.where(x > 8)[0]
    if len(qwe) > 0: kpmag[qwe] = 0.1*g[qwe] + 0.9*r[qwe]
    return kpmag
    
def kpm_bv(b,v):
    g = 0.54*b + 0.46*v - 0.07
    r = -0.44*b + 1.44*v+0.12
    kpmag = kpm_gr(g,r)
    return kpmag