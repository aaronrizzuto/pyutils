import numpy as np

def circmean(alpha,axis=None):
	mean_angle = np.arctan2(np.mean(np.sin(alpha),axis),np.mean(np.cos(alpha),axis))
	return mean_angle
 

def circvar(alpha,axis=None):
	if axis is None:
		N = alpha.size
	else:
		N = alpha.shape[axis]

	R = np.sqrt(np.sum(np.sin(alpha),axis)**2 + np.sum(np.cos(alpha),axis)**2)/N
	V = 1-R
	return V