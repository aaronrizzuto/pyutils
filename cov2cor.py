import numpy as np


def cov2cor(cov):

	dim1 = cov.shape[0]
	dim2 = cov.shape[1]
	corr = cov.copy()
	for i in range(0,dim1):
		sig_i = np.sqrt(cov[i,i])
		for j in range(0,dim2):
			sig_j = np.sqrt(cov[j,j])
			corr[i,j] = cov[i,j]/sig_i/sig_j
			
	return corr