import numpy as np

def rad2mas(rad):
    return rad*180/np.pi*1000.0*60*60
    
def mas2rad(mas):
    return mas/1000.0/60./60./180*np.pi