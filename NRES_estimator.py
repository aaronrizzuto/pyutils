import numpy as np
import pdb

def snr(vmag,time):

    ##define the known curve so far
    
    ttt = np.array([5.,5.,3.,20.,3.,15.])
    mag = np.array([6.29,6.27,7.95,11.07,6.93,8.51])
    sss = np.array([41.9,43.1,11.1,9.2,21.1,24.8])
    
    if np.max(vmag) > np.max(mag): print 'Beyond interpolation limit of data, be careful mate!'
    
        
    ##convert them all to unit time 
    sss = sss/np.sqrt(ttt)
    sorter = np.argsort(mag)
    mag = mag[sorter]
    sss = sss[sorter]
    ttt = ttt[sorter]
    
    unit_snr = np.interp(vmag,mag,sss)
    
    finalsnr = unit_snr*np.sqrt(time)
    
   
#    fluxfactor = 10.0**((-vmag+7.95)/2.5)
#    newsnr = 10.0*np.sqrt(fluxfactor)

    ##2. Adjust for time
#    finalsnr = newsnr*np.sqrt(time/3.0)
    
    return finalsnr

def etime(vmag,targsnr):
    getsnr = 0.0
    starttime = 60.0*2.0
    while getsnr < targsnr:
        getsnr = snr(vmag,starttime/60.0)
        starttime += 0.1*starttime
        if starttime >1499.9: getsnr=targsnr+1

    return starttime

def make_guide_plot(maglims,times):
##make a plot of snr for different mags and different exposure times.
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    mpl.rcParams['lines.linewidth']   = 3
    mpl.rcParams['axes.linewidth']    = 2
    mpl.rcParams['xtick.major.width'] =2
    mpl.rcParams['ytick.major.width'] =2
    mpl.rcParams['ytick.labelsize'] = 12
    mpl.rcParams['xtick.labelsize'] = 12
    mpl.rcParams['axes.labelsize'] = 18
    mpl.rcParams['legend.numpoints'] = 1
    mpl.rcParams['axes.labelweight']='semibold'
    mpl.rcParams['mathtext.fontset']='stix'
    mpl.rcParams['font.weight'] = 'bold'
    colo=['k','b','r','g','m','k--','b--','r--','g--','m--']
    for i in range(len(times)):
        themags = np.linspace(maglims[0],maglims[1],300.0)
        thissnr = snr(themags,times[i])
        plt.plot(themags,thissnr,colo[i],label=str(int(times[i])) + ' mins')
    
    plt.legend()
    plt.xlabel('mv (mags)')
    plt.ylabel('SNR')
    plt.tight_layout()
    
    plt.savefig('nres_snr.pdf')
    plt.show()
    
    return -1

