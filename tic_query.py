import sys,os,time,re,json,pprint,pdb
from astropy.table import Table
import numpy as np
import pandas as pd 
oo = pprint.PrettyPrinter(indent=4)
from gcirc import gcirc 

try: # Python 3.x
    from urllib.parse import quote as urlencode
    from urllib.request import urlretrieve
except ImportError:  # Python 2.x
    from urllib import pathname2url as urlencode
    from urllib import urlretrieve

try: # Python 3.x
    import http.client as httplib 
except ImportError:  # Python 2.x
    import httplib   


##ALL imports above here!


##general mast query format
def mastQuery(request):
    """Perform a MAST query.
    
        Parameters
        ----------
        request (dictionary): The Mashup request json object
        
        Returns head,content where head is the response HTTP headers, and content is the returned data"""
    
    server='mast.stsci.edu'

    # Grab Python Version 
    version = ".".join(map(str, sys.version_info[:3]))

    # Create Http Header Variables
    headers = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "text/plain",
               "User-agent":"python-requests/"+version}

    # Encoding the request as a json string
    requestString = json.dumps(request)
    requestString = urlencode(requestString)
    
    # opening the https connection
    conn = httplib.HTTPSConnection(server)

    # Making the query
    conn.request("POST", "/api/v0/invoke", "request="+requestString, headers)

    # Getting the response
    resp = conn.getresponse()
    head = resp.getheaders()
    content = resp.read().decode('utf-8')

    # Close the https connection
    conn.close()

    return head,content
    
def clean_dtypes(mds):
    ##reads in the mast data string in a way thats repeatable (I think)
    ##pandas does a weird job of this hence my custom version here
    #pdb.set_trace()
    jsobj = json.loads(mds)
    dts = []
    for ff in jsobj['fields']: 
        thisentry = [(ff['name'].encode("utf-8"),ff['type'].encode("utf-8"))]
        if ff['type'].encode("utf-8") == 'string': thisentry = [(ff['name'].encode("utf-8"),'|S50')]
        if ff['type'].encode("utf-8") == 'boolean': thisentry=[(ff['name'].encode("utf-8"),'int')]
        dts += thisentry
    ##we now have a list of dtypes that we should be using for each entry
    ##make emtpy recarray of correct size and type
    #pdb.set_trace()
    data = np.recarray(len(jsobj['data']),dtype=dts)
    #pdb.set_trace()
    ##fill it with the data
    for e in range(len(dts)):
        fieldname = dts[e][0]
        fieldtype = dts[e][1]
        for j in range(len(data)):
            if jsobj['data'][j][fieldname] == None: data[fieldname] = np.array([int(-99999)]).astype(fieldtype)[0]
            else: data[j][fieldname] = np.array([jsobj['data'][j][fieldname]]).astype(fieldtype)[0]
    #pdb.set_trace()
    return data
    
    
    
##ask mast to tell you the ra/dec of a named object   
def nameresolve(namestr):
    ##Given a name string, figure out what the coords are according to mast
    ##return -np.inf if object not found
    resolverRequest = {'service':'Mast.Name.Lookup',
                         'params':{'input':namestr,
                                  'format':'json'},
                        }
    
    headers,resolvedObjectString = mastQuery(resolverRequest)

    #pdb.set_trace()
    resolvedObject = json.loads(resolvedObjectString)

    
    if len(resolvedObject['resolvedCoordinate']) == 1:
        objRa = resolvedObject['resolvedCoordinate'][0]['ra']
        objDec = resolvedObject['resolvedCoordinate'][0]['decl']
    else: 
        objRa = -np.inf
        objDec = -np.inf
    return objRa,objDec

##Do a cone search around a target ra,dec 
def tic_radec_cone(ra,dec,radius,closest=False):
    '''
    Given ra,dec,coneradius, grab tic objects if you can
    ra,dec, in degrees  
    radius in mas  
    if closest == True, just return the closest object found in the TIC
    '''
    
    mastRequest = {'service':'Mast.Catalogs.Tic.Cone',
                'params':{'ra':ra,
                           'dec':dec,
                           'radius':radius/3600.0},
                 'format':'json',
                 'pagesize':2000,
                 'page':1,
                 'removenullcolumns':False,
                 'removecache':True}

    headers,mastDataString = mastQuery(mastRequest)
    
    #pdb.set_trace()
    ##lets make if a recarray, ACR likes recarrays...
    ##all cleaning and reading in the following line.
    data = clean_dtypes(mastDataString)   
    #jsobj = json.loads(mastDataString)
    #data2 = pd.DataFrame(jsobj['data']).to_records()       ##old line for reading in data
    #pdb.set_trace()
    if len(data) >= 1:
        if closest == True:
            dist = gcirc(ra,dec,data.ra,data.dec)
            close = np.where(dist == min(dist))[0]
            if len(close) > 1:
                print 'found two equally close, chose the first'
                #pdb.set_trace()
            close = close[0:1]
            return data[close]
    
    return data

##Get the TIC entry for a named target
def tic_name(namestr,radius):
    ##figure out the name, then do the tic search
    ra,dec = nameresolve(namestr)
    if ra < -1000:
        print 'name unresolved!! (' +namestr+')'  
        return []

    data = tic_radec(ra,dec,radius)
    dist = gcirc(ra,dec,data.ra,data.dec)

    ##now find the closest object
    closest = np.where(dist == min(dist))[0]
    if len(closest) > 1:
        print 'found two equally close, sending the first'
    closest = closest[0:1]
    
    return data[closest]
    
##if you known the tic number for something , grab its ra/dec
##useful for tessCut stuff

def tic_radec(ticnum):
    ##figure out the name, then do the tic search
    ra,dec = nameresolve('TIC'+str(ticnum))
    if ra < -1000:
        print 'name unresolved!!'
        return -100.,-90. 
    else:
        return ra,dec
    
##if you know the tic number for something, grab its full TIC entry 
def tic_tic(ticnum):
    ##figure out the name, then do the tic search
    ra,dec = nameresolve('TIC'+str(ticnum))
    if ra < -1000:
        print 'name unresolved!!'
        return -1 
    
    data = tic_radec(ra,dec,10.0)

    ##now find the closest object
    match = np.where(data.ID == ticnum)[0]
    if len(match) == 0: print 'somethings wrong here, no matches for tic ' + str(ticnum)
    return data[match]

##find out if a target is on the CTL list searching by ticnumber.
def ctl_tic(ra,dec,radius):
    
    mastRequest = {'service':'Mast.Catalogs.CTL.Cone',
                 'params':{'ra':ra,
                           'dec':dec,
                           'radius':radius/3600.0,
                           },
                 'format':'json',
                 'pagesize':2000,
                 'page':1,
                 'removenullcolumns':False,
                 'removecache':True}
    
    headers,mastDataString = mastQuery(mastRequest)
    data = clean_dtypes(mastDataString) 
    
    ##just tell the user if something is there:
    inctl = 0
    if len(data) > 0: inctl = 1 
    

    return inctl



def epic(ra,dec,radius):
    
    mastRequest = {'service':'Mast.Catalogs.epic.Cone',
                 'params':{'ra':ra,
                           'dec':dec,
                           'radius':radius/3600.0,
                           },
                 'format':'json',
                 'pagesize':2000,
                 'page':1,
                 'removenullcolumns':False,
                 'removecache':True}
    
    headers,mastDataString = mastQuery(mastRequest)
    import pdb
    pdb.set_trace()
    ##ra,dec = 242.561375,-19.31927222222222






