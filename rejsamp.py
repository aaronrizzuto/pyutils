import numpy as np
import time
def rejsamp(bins,hist,nsim):
    binsize = bins[1]-bins[0]
    bins    = bins+binsize/2.0
    minbins = min(bins)
    rngbins = max(bins)-minbins
    enough = False
    iter   = 0
    numbins = len(bins)
    bout = np.array([])
    tstart = time.clock()
    snum   = 10000
    while enough == False:
        iter = iter+1
        bsamp   = np.random.uniform(0.0,1.0,snum)*rngbins + minbins
        hsamp   = np.random.uniform(0.0,1.0,snum)*max(hist)
        bsamp_m = np.transpose(np.tile(bsamp,(numbins,1)))
        bins_m  = np.tile(bins,(snum,1))
        diffs   = bsamp_m - bins_m
        minspts    = np.argmin(abs(diffs),axis=1) 
        mins        = np.amin(abs(diffs),axis=1) 

        keep = np.where(hsamp-hist[minspts] < 0.0)
        bout = np.append(bout,bsamp[keep])
        if np.mod(iter,100) ==0:
            itime=time.clock()-tstart
            ntime = itime*nsim/max(len(bout),0.0000001)/60.
            if ntime > 1.0:    print "Time required:" +str(np.round(ntime,decimals=1)) +" mins"
        if len(bout) > nsim:
            enough=True
    bout = bout[0:nsim]
    return bout
    
def rejsamp_opt(bins,hist,nsim):
    binsize = bins[1]-bins[0]
    bins    = bins+binsize/2.0
    minbins = min(bins)
    rngbins = max(bins)-minbins
    enough = False
    iter   = 0
    numbins = len(bins)
    bout = np.zeros(nsim)
    wehave = 0
    tstart = time.clock()
    snum   = 10000
    while enough == False:
        bsamp   = np.random.uniform(0.0,1.0,snum)*rngbins + minbins
        hsamp   = np.random.uniform(0.0,1.0,snum)*max(hist)
        bsamp_m = np.transpose(np.tile(bsamp,(numbins,1)))
        bins_m  = np.tile(bins,(snum,1))
        diffs   = bsamp_m - bins_m
        minspts    = np.argmin(abs(diffs),axis=1) 
        mins        = np.amin(abs(diffs),axis=1) 

        keep = np.where(hsamp-hist[minspts] < 0.0)[0]
        if wehave + len(keep) > len(bout): keep = keep[0:len(bout)-wehave]            
        bout[wehave:len(keep)+wehave] = bsamp[keep]
        wehave += len(keep)
        #print wehave
        if (iter ==100) | (iter == 1000):
            itime=time.clock()-tstart
            ntime = itime*nsim/wehave/60.
            if ntime > 1.0:    print "Time required:" +str(np.round(ntime,decimals=1)) +" mins"
        
        if wehave >= nsim: enough=True
        iter = iter+1

    bout = bout[0:nsim]
    return bout
    
    
    
##sample with rejection sampling from a 2d distribution
##Note!!! THIS ASSUMES THE xgrid/ygrid points are the middle of the histogram bins!!!!!
##IN FUTURE WILL ADD OPTIONAL FOR WHEN THIS IS NOT THE CASE!!!
##NOTE2!! HIST MUST BE POSITIVE or zero (e.g. its a probability distribution )
def rejsamp_2d(xgrid,ygrid,hist,nsim,normalise=True,binmiddle=True):
    from scipy.interpolate import griddata    
    if normalise == True: hist /= np.max(hist)

    points = np.zeros((len(xgrid.flatten()),2),dtype=float)
    points[:,0] = xgrid.flatten()
    points[:,1] = ygrid.flatten()
    xrange = [np.min(xgrid),np.max(xgrid)]
    yrange = [np.min(ygrid),np.max(ygrid)]
    x1d = np.unique(xgrid)
    y1d = np.unique(ygrid)
    xbinsize = -x1d+np.roll(x1d,-1)
    ybinsize = -y1d+np.roll(y1d,-1)
    xbinsize[xbinsize < 0] = 0.0
    ybinsize[ybinsize < 0] = 0.0
    xbinsize /= xbinsize[0]
    ybinsize /= ybinsize[0]
    ##need these sizes for when the bins aren't uniformly spaced, to account for area factoring into the samplerate.

    bout = np.zeros((2,nsim),dtype=float)
    iter   = 0
    wehave = 0
    tstart = time.clock()
    snum   = 10000
    enough,wehave = False,0
    while enough == False: 
        xsample = np.random.uniform(0.0,1.0,snum)*(xrange[1]-xrange[0]) + xrange[0]
        ysample = np.random.uniform(0.0,1.0,snum)*(yrange[1]-yrange[0]) + yrange[0]
        hsamp   = np.random.uniform(0.0,1.0,snum)*np.max(hist)
       
        ##this doesnt really work...need to actually bin things
        xbins_m  = np.tile(x1d,(snum,1))
        ybins_m  = np.tile(y1d,(snum,1))
        xsamp_m  = np.transpose(np.tile(xsample,(xbins_m.shape[1],1)))
        ysamp_m  = np.transpose(np.tile(ysample,(ybins_m.shape[1],1)))

        ##now find the grid point in each that is the largest positive
        diffx = -xbins_m + xsamp_m
        diffy = -ybins_m + ysamp_m
        xpos  = np.argmin(diffx>=0,axis=1)-1
        ypos  = np.argmin(diffy>=0,axis=1)-1
        interph = hist[xpos,ypos].flatten()/xbinsize[xpos]/ybinsize[ypos]
        
        keep    = np.where(interph >= hsamp)[0]
        if wehave + len(keep) > bout.shape[1]: keep = keep[0:bout.shape[1]-wehave] 
        bout[0,wehave:len(keep)+wehave] = xsample[keep]
        bout[1,wehave:len(keep)+wehave] = ysample[keep]
        wehave += len(keep)
        
        if iter == 1000:
            itime=time.clock()-tstart
            ntime = itime*nsim/wehave/60.
            if ntime > 1.0:    print "Time required:" +str(np.round(ntime,decimals=1)) +" mins"
        
        if wehave >= nsim: enough=True
        iter = iter+1        
        
    return bout
    
    
    
    
    
    
    
    
    