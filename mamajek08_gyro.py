import numpy as np

def mamajek08(bv,p,a=0.407,b=0.325,c=0.495,n=0.566):

    f = a*(bv-c)**b
    tn = p/f
    t  = tn**(1.0/n)
    return t