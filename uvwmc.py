import numpy as np
import pdb

from ACRastro.gal_uvw import gal_uvw

def uvwmc(ra,dec,plx,pmra,pmdec,rv,sig_plx,sig_pmra,sig_pmdec,sig_rv,nsamp=10000,plxtype=True):
    rplx  =np.random.normal(loc=plx, scale=sig_plx, size=nsamp)
    rpmra =np.random.normal(loc=pmra, scale=sig_pmra, size=nsamp)    
    rpmde =np.random.normal(loc=pmdec, scale=sig_pmdec, size=nsamp)
    rrv   =np.random.normal(loc=rv, scale=sig_rv, size=nsamp)


    uu = np.zeros(nsamp)
    ww = np.zeros(nsamp)
    vv = np.zeros(nsamp)
    if plxtype == True:
        for i in range(nsamp):uu[i],vv[i],ww[i] = gal_uvw(ra=ra,dec=dec,plx=rplx[i],pmra=rpmra[i],pmdec=rpmde[i],vrad=rrv[i])
    if plxtype == False:
            for i in range(nsamp):uu[i],vv[i],ww[i] = gal_uvw(ra=ra,dec=dec,distance=rplx[i],pmra=rpmra[i],pmdec=rpmde[i],vrad=rrv[i])
    return np.mean(uu),np.mean(vv),np.mean(ww),np.std(uu),np.std(vv),np.std(ww)
    
    
def uvw6D(vect,cov,nsamp=10000):
    from gal_xyz import gal_xyz
    rsamp    = np.random.multivariate_normal(vect,cov,nsamp) ##nsampx6
    rx,ry,rz = gal_xyz(rsamp[:,0],rsamp[:,1],rsamp[:,2],radec=True,plx=True,reverse=False)

    uu = np.zeros(nsamp)
    ww = np.zeros(nsamp)
    vv = np.zeros(nsamp)
    for i in range(nsamp): uu[i],vv[i],ww[i] = gal_uvw(ra=rsamp[i,0],dec=rsamp[i,1],plx=rsamp[i,2],pmra=rsamp[i,3],pmdec=rsamp[i,4],vrad=rsamp[i,5])
    ##now calculate uvw and xyz samples from the above
    
    samps = np.array([rx,ry,rz,uu,vv,ww])

    return np.mean(samps,axis=1),np.cov(samps)